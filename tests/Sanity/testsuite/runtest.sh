#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/binutils/Sanity/testsuite
#
#   Description: The test rebuilds binutils.src.rpm and runs the suite.
#   The test is based on /tools/binutils/testsuite, but it had to be
#   rewritten.
#
#   Author: Michael Petlan <mpetlan@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2015 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

LD="${LD:-/usr/bin/ld}"
GCC="${GCC:-/usr/bin/gcc}"

PACKAGE="${PACKAGE:-$(rpm --qf '%{name}\n' -qf "$(which "$LD")" | head -1)}"
GCC_PACKAGE="${GCC_PACKAGE:-$(rpm --qf '%{name}\n' -qf "$(which "$GCC")" | head -1)}"

PACKAGES="${PACKAGES:-$PACKAGE}"
REQUIRES="${REQUIRES:-$GCC_PACKAGE}"

rlJournalStart
	rlPhaseStartSetup
    ARCH="$(rlGetPrimaryArch)"

    if [ "$ARCH" = "x86_64" ]; then
        BUILD_ARCHS="aarch64 ppc64le s390x x86_64"
    else
        BUILD_ARCHS="$ARCH"
    fi

    rlLogInfo "PACKAGES=$PACKAGES"
    rlLogInfo "REQUIRES=$REQUIRES"
    rlLogInfo "COLLECTIONS=$COLLECTIONS"
    rlLogInfo "LD=$LD"
    rlLogInfo "GCC=$GCC"
    rlLogInfo "ARCH=$ARCH"
    rlLogInfo "BUILD_ARCHS=$BUILD_ARCHS"

		rlAssertRpm --all

		# temp dir
    rlRun "TESTDIR=$PWD"

		rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"

    rlRun "LOGDIR=$TmpDir/LOGS"
    rlRun "BUILDDIR=$(rpm --define='_topdir $TmpDir' --eval=%_builddir)"
    rlRun "CURRENT_BUILD=${BUILDDIR}/binutils-$(rpmquery "$PACKAGE" --queryformat=%{VERSION})"

    rlRun "mkdir $LOGDIR"
  
    rlRun "pushd $TmpDir"

		# fetch'n'build the source
		rlFetchSrcForInstalled "$PACKAGE"
		rlRun "yum-builddep -y *.src.rpm"
    rlRun "SRPM=$(find . -name 'binutils-*.src.rpm')"
    rlRun "rpm -ivh --define='_topdir $TmpDir' $SRPM"
    rlRun "SPECFILE=$(find "$TmpDir/" -name '*.spec')"
    rlRun "dnf builddep -y $SPECFILE"
  rlPhaseEnd

  rlPhaseStartSetup "Rebuild binutils"
		rlRun "rpmbuild -bc --define='_topdir $TmpDir' $SPECFILE"

    for build_arch in $BUILD_ARCHS; do
        rlRun "cp $CURRENT_BUILD/build-${build_arch}-redhat-linux/binutils/binutils.log $LOGDIR/binutils-$build_arch.log"
        rlRun "cp $CURRENT_BUILD/build-${build_arch}-redhat-linux/binutils/binutils.sum $LOGDIR/binutils-$build_arch.sum"

        if [ "$build_arch" = "$ARCH" ]; then
            rlRun "cp $CURRENT_BUILD/build-${build_arch}-redhat-linux/ld/ld.log $LOGDIR/ld-$build_arch.log"
            rlRun "cp $CURRENT_BUILD/build-${build_arch}-redhat-linux/ld/ld.sum $LOGDIR/ld-$build_arch.sum"
        fi

        rlRun "cp $CURRENT_BUILD/build-${build_arch}-redhat-linux/gas/testsuite/gas.log $LOGDIR/gas-$build_arch.log"
        rlRun "cp $CURRENT_BUILD/build-${build_arch}-redhat-linux/gas/testsuite/gas.sum $LOGDIR/gas-$build_arch.sum"
    done
	rlPhaseEnd

  	for TOOL in binutils ld gas; do
        for build_arch in $BUILD_ARCHS; do
            if [[ "$TOOL" == "ld" && "$build_arch" != "$ARCH" ]]; then
                continue
            fi

            tool_label="$TOOL / $build_arch"
            tool_log="$LOGDIR/$TOOL-$build_arch"

            rlPhaseStartTest "$tool_label"
                rlLogInfo "$tool_label Summary"
                rlLogInfo "$(grep -A 50 '=== .* Summary ===' "$tool_log.sum")"

                rlRun "awk \"/=== $TOOL tests ===/,/=== $TOOL Summary ===/\" $tool_log.sum > $tool_log.tests" 0,1 "Save results of all tests"
                rlRun "grep -E '^FAIL: ' $tool_log.tests | sort > $tool_log.failed" 0,1 "Save failures"

                rlRun "grep '# of unexpected failures' $tool_log.sum" 0,1 "Checking number of unexpected failures"
                rlLogInfo "$(sort < "$tool_log.failed")"
                rlLogInfo "$(grep -E '^[A-Z]+:' "$tool_log.tests" | sort)"
            rlPhaseEnd
	      done
    done

    rlPhaseStartCleanup
        rlRun "tar czf $TmpDir/logs.tgz $LOGDIR/*.sum $LOGDIR/*.log $LOGDIR/*.tests $LOGDIR/*.failed"
        rlRun "tar czf $TmpDir/buildroot.tgz $CURRENT_BUILD/"
        rlFileSubmit logs.tar.gz
        rlFileSubmit buildroot.tgz
        rlRun "popd" # $TmpDir
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
